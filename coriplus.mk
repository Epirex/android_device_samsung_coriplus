# Overlay		
DEVICE_PACKAGE_OVERLAYS += \
        device/samsung/coriplus/overlay

# This device is LDPI
PRODUCT_AAPT_CONFIG := normal ldpi mdpi nodpi
PRODUCT_AAPT_PREF_CONFIG := ldpi

# Init files
PRODUCT_COPY_FILES += \
	device/samsung/coriplus/rootdir/init.rhea_ss_coriplus.rc:root/init.rhea_ss_coriplus.rc \
	device/samsung/coriplus/rootdir/init.bcm2165x.usb.rc:root/init.bcm2165x.usb.rc \
	device/samsung/coriplus/rootdir/init.log.rc:root/init.log.rc \
	device/samsung/coriplus/rootdir/init.bt.rc:root/init.bt.rc \
	device/samsung/coriplus/rootdir/lpm.rc:root/lpm.rc \
	device/samsung/coriplus/rootdir/ueventd.rhea_ss_coriplus.rc:root/ueventd.rhea_ss_coriplus.rc

# Vold
PRODUCT_COPY_FILES += \
	device/samsung/coriplus/configs/vold.conf:system/etc/vold.conf \
	device/samsung/coriplus/configs/vold.fstab:system/etc/vold.fstab

# Prebuilt
PRODUCT_COPY_FILES += \
	device/samsung/coriplus/prebuilt/sec_param.ko:root/sec_param.ko \

# Filesystem management tools
PRODUCT_PACKAGES += \
	setup_fs

# Usb accessory
PRODUCT_PACKAGES += \
	com.android.future.usb.accessory

# Audio
PRODUCT_PACKAGES += \
	audio.usb.default \
	audio.a2dp.default \
        audio.policy.rhea \
        hwprops

PRODUCT_COPY_FILES += \
	device/samsung/coriplus/configs/asound.conf:system/etc/asound.conf \
	device/samsung/coriplus/configs/audio_effects.conf:system/etc/audio_effects.conf

# Wi-Fi
PRODUCT_PACKAGES += \
        dhcpcd.conf \
        hostapd \
        wpa_supplicant

PRODUCT_PROPERTY_OVERRIDES += \
    wifi.interface=wlan0 \
    mobiledata.interfaces=rmnet0

PRODUCT_COPY_FILES += \
	device/samsung/coriplus/configs/wpa_supplicant.conf:system/etc/wifi/wpa_supplicant.conf

# GPS
PRODUCT_COPY_FILES += \
	device/samsung/coriplus/configs/gps.conf:system/etc/gps.conf

$(call inherit-product, device/common/gps/gps_eu_supl.mk)

# Media
PRODUCT_COPY_FILES += \
	device/samsung/coriplus/configs/media_profiles.xml:system/etc/media_profiles.xml

# Misc Packages
PRODUCT_PACKAGES += \
	SamsungServiceMode 

# Charger
PRODUCT_PACKAGES += \
	charger \
	charger_res_images

# These are the hardware-specific features
PRODUCT_COPY_FILES += \
	frameworks/base/data/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
	frameworks/base/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
	frameworks/base/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
	frameworks/base/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
	frameworks/base/data/etc/android.hardware.location.xml:system/etc/permissions/android.hardware.location.xml \
	frameworks/base/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
	frameworks/base/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
        frameworks/base/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/base/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
	frameworks/base/data/etc/android.hardware.sensor.compass.xml:system/etc/permissions/android.hardware.sensor.compass.xml \
	frameworks/base/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
	frameworks/base/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
	frameworks/base/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
	frameworks/base/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/base/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
	frameworks/base/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
	packages/wallpapers/LivePicker/android.software.live_wallpaper.xml:system/etc/permissions/android.software.live_wallpaper.xml

# Browser
PRODUCT_PACKAGES += \
	libskia_legacy

# RIL
PRODUCT_PROPERTY_OVERRIDES += \
	ro.telephony.ril_class=SamsungBCMRIL \
	persist.radio.multisim.config=dsds \
	ro.telephony.call_ring.multiple=0 \
	ro.telephony.call_ring=0

# Disable error Checking
PRODUCT_PROPERTY_OVERRIDES += \
	ro.kernel.android.checkjni=0 \
	dalvik.vm.checkjni=false

# MTP
#PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
#	persist.sys.usb.config=mtp

# Root and USB
ADDITIONAL_DEFAULT_PROPERTIES += \
    ro.secure=0 \
    ro.adb.secure=0 \
    persist.service.adb.enable=1 \
    persist.service.debuggable=1

PRODUCT_PROPERTY_OVERRIDES += \
    persist.service.adb.enable=1 \
    persist.service.debuggable=1 \
    persist.sys.usb.config=mtp,adb

# Misc
PRODUCT_TAGS += dalvik.gc.type-precise
$(call inherit-product, frameworks/base/build/phone-hdpi-512-dalvik-heap.mk)

# Vendor
$(call inherit-product-if-exists, vendor/samsung/coriplus/coriplus-vendor.mk)
