## Specify phone tech before including full_phone
$(call inherit-product, vendor/cm/config/gsm.mk)

# Release name
PRODUCT_RELEASE_NAME := GT-S5301

# Bootanimation
TARGET_SCREEN_HEIGHT := 320
TARGET_SCREEN_WIDTH := 240

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/samsung/coriplus/coriplus.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := coriplus
PRODUCT_NAME := cm_coriplus
PRODUCT_BRAND := samsung
PRODUCT_MODEL := GT-S5301
PRODUCT_MANUFACTURER := samsung

#Set build fingerprint / ID / Prduct Name ect.
PRODUCT_BUILD_PROP_OVERRIDES += PRODUCT_NAME=coriplus TARGET_DEVICE=coriplus
