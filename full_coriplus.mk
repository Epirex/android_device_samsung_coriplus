# Inherit from those products. Most specific first.
$(call inherit-product, device/samsung/coriplus/coriplus.mk)

# Discard inherited values and use our own instead.
PRODUCT_NAME := full_coriplus
PRODUCT_DEVICE := coriplus
PRODUCT_BRAND := samsung
PRODUCT_MANUFACTURER := samsung
PRODUCT_MODEL := GT-S5301
